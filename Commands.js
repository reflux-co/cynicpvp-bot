
const Discord = require("discord.js");
const Message = Discord.Message;

const { client } = require("./cynic");

const embedColor = 0x5137e5;

/**
 * Command structure for commands (duh)
 */
class CommandStruct {

    static get prefix() {
        return '!';
    }

    /**
     * @param {Message} message 
     */
    execute(message){}

    /**
     * @param {Message} message 
     */
    isValidExecution(message){return false;}

    /**
     * @param {string} name 
     */
    constructor (name) {
        this.name = name;
    }

};

// !announce
class AnnounceCommand extends CommandStruct {

    get data() {
        return {
            parentGuildId: '568628662639788032', // CynicPvP Server ID
            channelId: '569900745428697108', // #announce ID
            announcementsChannelId: '568912715234410506'
        }
    };

    /**
     * @param {Message} message 
     */
    async execute(message) {
        let channel = message.channel;
        // ask whether or not to ping @everyone
        let msg = await channel.send(new Discord.RichEmbed({
            title: "Announcement Process",
            description: "Would you like to ping @everyone?",
            color: embedColor
        }));
        msg.react("❌");
        msg.react("✔"); // react
        // collect reactions
        let collector = msg.createReactionCollector((reaction, user) => 
                user.id === message.author.id && (reaction.emoji.name === "✔" || reaction.emoji.name === "❌"), { time: 15000 });
        collector.on("collect", async react => {
            react.message.delete();
            // check if @everyone
            let atEveryone = react.emoji.name === "✔";
            // otherwise send message asking for the announcement
            let messageToDel = await channel.send(new Discord.RichEmbed({
                title: "Announcement Content",
                description: "Please format your announcement like this:\n```\nTitle\nThe Content```",
                color: embedColor
            }));
            // collect next message
            let messageCollector = channel.createMessageCollector(m => m.author.id === message.author.id, { maxMatches: 1 });
            messageCollector.on("collect", m => {
                messageToDel.delete();
                m.delete();
                if (m.content.indexOf("\n") === -1) {
                    return;
                }
                let split = m.content.split("\n", 2);
                // send out announcement
                let announcementsChannel = message.guild.channels.find(c => c.id === this.data.announcementsChannelId);
                if (announcementsChannel === null) {
                    return;
                }
                
                announcementsChannel.send((atEveryone) ? message.guild.roles.find(r => r.name === "@everyone") : "", {
                    embed: new Discord.RichEmbed({
                        title: split[0],
                        description: split[1],
                        color: embedColor,
                        footer: {
                            text: `Announcement from ${message.author.tag} | Bot by luaq.#1337 <3`
                        }
                    })
                });
            });
        });
    }

    /**
     * @param {Message} message 
     */
    isValidExecution(message) {
        return message.channel.id === this.data.channelId && message.guild.id === this.data.parentGuildId;
    }

    constructor() {
        super("announce");
    }

};

// !accept
class AcceptCommand extends CommandStruct {
    
    get adminChannelId() {
        return '569401558727393300';
    }

    /**
     * @param {Message} message 
     */
    execute(message) {
        let mention = message.mentions.members.first();
        if (mention !== null) {
            mention.user.send(new Discord.RichEmbed({
                title: "You were accepted!",
                description: `Upon careful review, we have decided that you just might be a good fit for our staff team. If you can do an interview, please contact ${message.author} to arrange an interview.\n\nThanks for applying!`,
                color: embedColor
            }));
        }
    }

    /**
     * @param {Message} message 
     */
    isValidExecution(message) {
        return message.mentions.members.first !== null && message.channel.id === this.adminChannelId;
    }

    constructor () {
        super("accept");
    }

};

module.exports = {
    CommandStruct,

    commandList: [
        new AnnounceCommand(),
        new AcceptCommand()
    ]
};
