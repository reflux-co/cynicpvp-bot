
const Discord = require("discord.js");
const client = new Discord.Client(); // bot client object

const { CommandStruct, commandList } = require("./Commands"); // get list of commands (and struct)

const playings = [
    "cynicpvp.net",
    "https://luaq.dev"
];
let i = 0;

function updatePlaying() {
    // set playing
    client.user.setPresence({
        status: "online",
        game: {
            name: playings[i],
            type: "PLAYING"
        }
    });

    if (++i >= playings.length) {
        i = 0;
    } // next playing message

    // update every 60 seconds
    setTimeout(updatePlaying, 60 * 1000);
}

client.on('ready', () => {
    updatePlaying();

    // log the login
    console.log(`Login success: ${client.user.tag}`);
});

// command system
client.on("message", async message => {
    if (message.author.bot || !message.content.startsWith(CommandStruct.prefix)) {
        return; // this comment doesn't make sense unless you check the commit history
    }
    
    // find command
    let command = commandList.find(x => message.content.substring(CommandStruct.prefix.length).startsWith(x.name));
    // make sure it exists
    if (command !== undefined) {
        if (!command.isValidExecution(message)) {
            return; // it shouldn't run
        }
        // run command
        message.delete();
        command.execute(message);
    }

});

login = async () => {
    return await client.login(process.env.BOT_TOKEN); // To properly login open the directory where this script is located in the terminal and type set BOT_TOKEN=%the_token_from_the_developer_portal%
};

login();

module.exports = {
    client
};
